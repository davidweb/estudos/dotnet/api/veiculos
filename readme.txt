O Visual Studio adicionou as dependências conjunto completo de de ASP.NET MVC 5 ao projeto 'APIAngular'. 

O arquivo Global.asax.cs no projeto requer as alterações adicionais para habilitar ASP.NET MVC.

1. Adicione as seguintes referências do namespace:

    using System.Web.Mvc;
    using System.Web.Routing;
    using System.Web.Optimization;

2. Se o código já não definir um método Application_Start, adicione o seguinte método:

    protected void Application_Start()
    {
    }

3. Adicione as seguintes linhas ao final do método Application_Start:

    AreaRegistration.RegisterAllAreas();
    RouteConfig.RegisterRoutes(RouteTable.Routes);
    FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
    BundleConfig.RegisterBundles(BundleTable.Bundles);