﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using APIAngular.Models;

namespace APIAngular.Controllers
{
    [RoutePrefix("api/veiculo")]
    public class VeiculoController : ApiController
    {
        List<VeiculoModel> list = new List<VeiculoModel>();

        [Route("getVeiculos")]
        public List<VeiculoModel> getVeiculos()
        {
            todosVeiculos();
            return list;
        }

        [Route("addVeiculo")]
        public List<VeiculoModel> addVeiculo([FromBody]VeiculoModel veiculo)
        {
            list.Add(veiculo);
            todosVeiculos();

            return list;
        }

        public List<VeiculoModel> todosVeiculos()
        {
            list.Add(new VeiculoModel()
            {
                idVeiculo = 1,
                placaVeiculo = "OVP9026",
                corVeiculo = "Preto",
                marcaVeiculo = "VW",
                modeloVeiculo = "Fox"
            });

            list.Add(new VeiculoModel()
            {
                idVeiculo = 2,
                placaVeiculo = "OOX1806",
                corVeiculo = "Branco",
                marcaVeiculo = "Ford",
                modeloVeiculo = "Ranger"
            });

            return list;
        }
    } 
}